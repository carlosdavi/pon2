
#Aluno: Carlos David Lira


import datetime
import os
from copy import deepcopy
import sys
import queue


class NoArvoreB:
    def __init__(self, degree=1, numeroKey=0, items=None, filho=None,
                 index=None):

        self.numberOfKeys = numeroKey
        if items != None:
            self.items = items
        else:
            self.items = [None] * 2 * degree
        if filho != None:
            self.child = filho
        else:
            self.child = [None] * (2 * degree + 1)
        self.index = index

    def __repr__(self):

        return "NoArvoreB(" + str(len(self.items) // 2) + "," + str(self.numberOfKeys) + \
               "," + repr(self.items) + "," + repr(self.child) + "," + str(self.index) + ")\n"

    def __str__(self):
        st = 'The contents of the node with index ' + \
             str(self.index) + ':\n'
        for i in range(0, self.numberOfKeys):
            st += '   Index   ' + str(i) + '  >  child: '
            st += str(self.child[i])
            st += '   item: '
            st += str(self.items[i]) + '\n'
        st += '                 child: '
        st += str(self.child[self.numberOfKeys]) + '\n'
        return st

    def insert(self, bTree, item):

        pass

    def splitNode(self, bTree, item, right):

        pass

    def getLeftMost(self, bTree):

        if self.child[0] == None:
            return self.items[0]

        return bTree.nodes[self.child[0]].getLeftMost(bTree)

    def delete(self, bTree, item):

        pass

    def redistributeOrCoalesce(self, bTree, childIndex):

        pass

    def getChild(self, i):

        if (0 <= i <= self.numberOfKeys):
            return self.child[i]
        else:
            print('Error in getChild().')

    def setChild(self, i, childIndex):

        self.child[i] = childIndex

    def getIndex(self):
        return self.index

    def setIndex(self, anInteger):
        self.index = anInteger

    def isFull(self):

        return (self.numberOfKeys == len(self.items))

    def getNumberOfKeys(self):
        return self.numberOfKeys

    def setNumberOfKeys(self, anInt):
        self.numberOfKeys = anInt

    def clear(self):
        self.numberOfKeys = 0
        self.items = [None] * len(self.items)
        self.child = [None] * len(self.child)

    def search(self, bTree, item):

        pass


class BTree:
    def __init__(self, degree, nodes={}, rootIndex=1, freeIndex=2):
        self.degree = degree

        if len(nodes) == 0:
            self.rootNode = NoArvoreB(degree)
            self.nodes = {}
            self.rootNode.setIndex(rootIndex)
            self.writeAt(1, self.rootNode)
        else:
            self.nodes = deepcopy(nodes)
            self.rootNode = self.nodes[rootIndex]

        self.rootIndex = rootIndex
        self.freeIndex = freeIndex

    def __repr__(self):
        return "BTree(" + str(self.degree) + ",\n " + repr(self.nodes) + "," + \
               str(self.rootIndex) + "," + str(self.freeIndex) + ")"

    def __str__(self):
        st = '  O elemento da Arvore e ' + str(self.degree) + \
             '.\n'
        st += '  O indice no no principal e ' + \
              str(self.rootIndex) + '.\n'
        for x in range(1, self.freeIndex):
            node = self.readFrom(x)
            if node.getNumberOfKeys() > 0:
                st += str(node)
        return st

    def delete(self, anItem):

        pass

    def getFreeIndex(self):

        self.freeIndex += 1
        return self.freeIndex - 1

    def getFreeNode(self):

        newNode = NoArvoreB(self.degree)
        index = self.getFreeIndex()
        newNode.setIndex(index)
        self.writeAt(index, newNode)
        return newNode

    def inorderOn(self, aFile):

        aFile.write("\n")
        self.inorderOnFrom(aFile, self.rootIndex)

    def inorderOnFrom(self, aFile, index):

        pass

    def insert(self, anItem):

        pass

    def levelByLevel(self, aFile):

        pass

    def readFrom(self, index):

        if self.nodes.__contains__(index):
            return self.nodes[index]
        else:
            return None

    def recycle(self, aNode):

        aNode.clear()

    def retrieve(self, anItem):

        pass

    def __searchTree(self, anItem):

        pass

    def update(self, anItem):

        pass

    def writeAt(self, index, aNode):

        self.nodes[index] = aNode


def btreemain():
    print("My/Our name(s) is/are ")

    lst = [10, 8, 22, 14, 12, 18, 2, 50, 15]

    b = BTree(2)

    for x in lst:
        print(repr(b))
        print("***Inserting", x)
        b.insert(x)

    print(repr(b))

    lst = [14, 50, 8, 12, 18, 2, 10, 22, 15]

    for x in lst:
        print("***Deleting", x)
        b.delete(x)
        print(repr(b))

    # return
    lst = [54, 76]

    for x in lst:
        print("***Deleting", x)
        b.delete(x)
        print(repr(b))

    print("***Inserting 14")
    b.insert(14)

    print(repr(b))

    print("***Deleting 2")
    b.delete(2)

    print(repr(b))

    print("***Deleting 84")
    b.delete(84)

    print(repr(b))


def readRecord(file, recNum, recSize):
    file.seek(recNum * recSize)
    record = file.read(recSize)
    return record


def readField(record, colTypes, fieldNum):
    offset = 0
    for i in range(fieldNum):
        colType = colTypes[i]

        if colType == "int":
            offset += 10
        elif colType[:4] == "char":
            size = int(colType[4:])
            offset += size
        elif colType == "float":
            offset += 20
        elif colType == "datetime":
            offset += 24

    colType = colTypes[fieldNum]

    if colType == "int":
        value = record[offset:offset + 10].strip()
        if value == "null":
            val = None
        else:
            val = int(value)
    elif colType == "float":
        value = record[offset:offset + 20].strip()
        if value == "null":
            val = None
        else:
            val = float(value)
    elif colType[:4] == "char":
        size = int(colType[4:])
        value = record[offset:offset + size].strip()
        if value == "null":
            val = None
        else:
            val = value[1:-1]  # remove the ' and ' from each end of the string
            if type(val) == bytes:
                val = val.decode("utf-8")
    elif colType == "datetime":
        value = record[offset:offset + 24].strip()
        if value == "null":
            val = None
        else:
            if type(val) == bytes:
                val = val.decode("utf-8")
            val = datetime.datetime.strptime(val, '%m/%d/%Y %I:%M:%S %p')
    else:
        print("Unrecognized Type")
        raise Exception("Unrecognized Type")

    return val


class Item:
    def __init__(self, key, value):
        self.key = key
        self.value = value

    def __repr__(self):
        return "Item(" + repr(self.key) + "," + repr(self.value) + ")"

    def __eq__(self, other):
        if type(self) != type(other):
            return False

        return self.key == other.key

    def __lt__(self, other):
        return self.key < other.key

    def __gt__(self, other):
        return self.key > other.key

    def __ge__(self, other):
        return self.key >= other.key

    def getValue(self):
        return self.value

    def getKey(self):
        return self.key


def main():
    # Select Feed.FeedNum, Feed.Name, FeedAttribType.Name, FeedAttribute.Value where
    # Feed.FeedID = FeedAttribute.FeedID and FeedAttribute.FeedAtribTypeID = FeedAttribType.ID
    attribTypeCols = ["int", "char20", "char60", "int", "int", "int", "int"]
    feedCols = ["int", "int", "int", "char50", "datetime", "float", "float", "int", "char50", "int"]
    feedAttributeCols = ["int", "int", "float"]

    feedAttributeTable = open("FeedAttribute.tbl", "r")

    if os.path.isfile("Feed.idx"):
        indexFile = open("Feed.idx", "r")
        feedTableRecLength = int(indexFile.readline())
        feedIndex = eval("".join(indexFile.readlines()))
    else:
        feedIndex = BTree(3)
        feedTable = open("Feed.tbl", "r")
        offset = 0
        for record in feedTable:
            feedID = readField(record, feedCols, 0)
            anItem = Item(feedID, offset)
            feedIndex.insert(anItem)
            offset += 1
            feedTableRecLength = len(record)

        print("Feed Table Index Created")
        indexFile = open("Feed.idx", "w")
        indexFile.write(str(feedTableRecLength) + "\n")
        indexFile.write(repr(feedIndex) + "\n")
        indexFile.close()

    if os.path.isfile("FeedAttribType.idx"):
        indexFile = open("FeedAttribType.idx", "r")
        attribTypeTableRecLength = int(indexFile.readline())
        attribTypeIndex = eval("".join(indexFile.readlines()))
    else:
        attribTypeIndex = BTree(3)
        attribTable = open("FeedAttribType.tbl", "r")
        offset = 0
        for record in attribTable:
            feedAttribTypeID = readField(record, attribTypeCols, 0)
            anItem = Item(feedAttribTypeID, offset)
            attribTypeIndex.insert(anItem)
            offset += 1
            attribTypeTableRecLength = len(record)

        print("Attrib Type Table Index Created")
        indexFile = open("FeedAttribType.idx", "w")
        indexFile.write(str(attribTypeTableRecLength) + "\n")
        indexFile.write(repr(attribTypeIndex) + "\n")
        indexFile.close()

    feedTable = open("Feed.tbl", "rb")
    feedAttribTypeTable = open("FeedAttribType.tbl", "rb")
    before = datetime.datetime.now()
    for record in feedAttributeTable:
        feedID = readField(record, feedAttributeCols, 0)
        feedAttribTypeID = readField(record, feedAttributeCols, 1)
        value = readField(record, feedAttributeCols, 2)

        lookupItem = Item(feedID, None)
        item = feedIndex.retrieve(lookupItem)
        offset = item.getValue()
        feedRecord = readRecord(feedTable, offset, feedTableRecLength)
        feedNum = readField(feedRecord, feedCols, 2)
        feedName = readField(feedRecord, feedCols, 3)

        lookupItem = Item(feedAttribTypeID, None)
        item = attribTypeIndex.retrieve(lookupItem)
        offset = item.getValue()
        feedAttribTypeRecord = readRecord(feedAttribTypeTable, offset, \
                                          attribTypeTableRecLength)
        feedAttribTypeName = readField(feedAttribTypeRecord, attribTypeCols, 1)

        print(feedNum, feedName, feedAttribTypeName, value)
    after = datetime.datetime.now()
    deltaT = after - before
    milliseconds = deltaT.total_seconds() * 1000
    print("Done. The total time for the query with indexing was", milliseconds, \
          "milliseconds.")


if __name__ == "__main__":
    main()