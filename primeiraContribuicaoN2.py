import matplotlib.pyplot as plt
import timeit
import random


def pesquisarNumero(lst):
    ind = random.randint(0, len(lst) - 1)
    return lst[ind]


def buscaLinear(lst, key):
    for i in range(len(lst) - 1):
        if (i == key):
            return True
    return False


def buscarBin(lst, key):
    esq = 0
    dire = len(lst) - 1
    while (esq <= dire):
        pmed = (esq + dire) // 2
        if (lst[pmed] == key): return True
        if (lst[pmed] < key):
            esq = pmed + 1
        else:
            dire = pmed - 1
    return False


def listaRand(tamanho, lst):
    random.seed()
    i = 0
    while i < tamanho:
        num = random.randint(1, 10 * tamanho)
        if (num not in lst):
            lst.append(num)
        i += 1


numerosDaLista = [6000, 9000, 12000, 15000, 24000]

tLinear = [list() for _ in range(5)]
tBinaria = [list() for _ in range(5)]
tDiferenca = [list() for _ in range(5)]

number = 0

for i in numerosDaLista:

    for j in range(10):
        lst = []
        listaRand(i, lst)
        sorted(lst)
        numberKey = pesquisarNumero(lst)
        tLinear[number].append(
            timeit.timeit("findLinear({},{})".format(lst, numberKey), setup="from __main__ import findLinear",
                          number=1))
        tBinaria[number].append(
            timeit.timeit("findBin({},{})".format(lst, numberKey), setup="from __main__ import findBin", number=1))
        dif = tLinear[number][j] - tBinaria[number][j]
        tDiferenca[number].append(dif)

    number = number + 1

tempoMedioLin = []
tempoMedioBin = []
tempoMedioDaDiferenca = []

for i in range(5):
    somalinear = 0
    somabinaria = 0
    somadif = 0
    for j in range(10):
        somalinear += tLinear[i][j]
        somabinaria += tBinaria[i][j]
        somadif += tDiferenca[i][j]
    tempoMedioLin.append(somalinear / 10)
    tempoMedioBin.append(somabinaria / 10)
    tempoMedioDaDiferenca.append(somadif / 10)

plt.plot(numerosDaLista, tempoMedioLin, label="Lin")
plt.plot(numerosDaLista, tempoMedioBin, label="Bin")
plt.plot(numerosDaLista, tempoMedioDaDiferenca, label="Diferença")
plt.legend()

plt.xlabel('Números da lista')
plt.ylabel('Tempo')
plt.title('Diferenças das pesquisas...')
plt.show()